Instructions for running the project:

-Specify the M, N, O values, the input, output directory paths in "runit.sh" file.
-Specify the nodes, walltime in wikispikes.pbs accordingly. The default values in this project are 16 nodes and 6:00 hours.
-Run the project by executing "./runit.sh" command.

-Input path specified in our project is : /lustre/$USER/wikistats/2013-01/pagecounts-201301* 

-The output file for 31 days is included in this folder.
-The project was tried running for 60 days of input files (on 16 nodes) but got an exception of disk space error. And when tried for 24 nodes, the job stayed in the batch queue for a very long time and it was aborted by PBS Server.

-Input format:
<lang_name> <page_name> <#page_views> <page_size>

Example:
fr Justin_Bieber 105 2237717
ab %D0%90%D1%84%D0%B0%D0%B8%D0%BB:Coat_of_arms_of_Burkina_Faso.svg 1 12152

-Output format:
<Top N spiked pages for the top most language> <max_spikes>
<Topmost_language> <unique_page_count>
<Top N spiked pages for the next top most language> <max_spikes>
<Next_Topmost_language> <unique_page_count>
<Top N spiked pages for the next top most language> <max_spikes>
<Next_Topmost_language> <unique_page_count>
.
.
.

Example:

Jodie_Foster	451141
Colin_Kaepernick	208598
Special:Random	184358
Main_Page	157578
Ray_Lewis	132891
Special:Search	110376
File:Gray319.png	108176
Bryan_Cranston	101310
Girls_(TV_series)	100226
Effect_of_psychoactive_drugs_on_animals	88501
en	166282684
Wikipedia:Hauptseite	318987
Trigeminusneuralgie	128502
Andreas_Dahl	95354
Tortilla	66613
Johannes_Aventinu	50138
Helmut_Berger_(1944)	49276
Sandro_Cortese	37198
Hotel_Adlon	32908
Allegra_Curtis	32702
Der_Ghostwriter	32425
de	28164838




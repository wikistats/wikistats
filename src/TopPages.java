/**    
 * Class Name: TopPages.java                                                                                                                 
 * This is the second MapReduce class which takes the input from first MapReduce and outputs the top N
 * spiked pages along with the #uniquePages in a particular language.                                                                            
 * Map 2:                                                                                                                                      * The Mapper class takes the input data from the Reduce 1. Process i/p and makes the 
 * language unique by merging the page_name with the maximum spike as value.
 * Input:
 * Key: offset
 * Value: entire record/line in the input file
 *                                                                                                 
 * Output:                                                                                                                                    
 * Key: <language_name>                                                                                                        
 * Value: <<page_name>" "<maxspike>>                                                                                                           * O/p Eg:<Key><Value>                                                                                                                         * <aa> <<Main_Page>" "765843>                                                                                                                
 * <en> <<Cricket>" "836865>                                                                                                                  
 *                                                                                                                                             
 * Reduce 2:                                                                                                                                   * The languages become unique by now. Compute the top N spiked pages in that language along with
 * no of unique pages in that language
 * Input:                                                                                                                                      * Key: <language_name>                                                                                                        
 * Value: <<page_name>" "<maxspike>>                                                                                                            
 * Output:                                                                                                                                 
 * Key: lang                                                                                                                           
 * Value: pagename1 spike1 pagename2 spike2 ......#uniquepages                                                                                                                             
 * O/p Eg:<Key><Value>                                                                                                                         * <aa><MoiraMoira 4490 Main_Page 3445.... 74856765>                                                                                           * <fr><MoiraMoirab 4420 Main_Page 845.... 8745875>
 *
 * @authors: Ranga Reddy, Sakethram
 */


package team4.project1;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

//@authors : Ranga Reddy, Sakethram
public class TopPages {
    
    //@author: Ranga Reddy
    public static class TopPagesMap
	extends Mapper<LongWritable, Text, Text, Text> {
	//In the above statement, LongWritable, Text indicates the input Key Value formats
	//The last two Text, Text indicates the output Key Value formats for the mapper
	
	/*
	  This map takes the output from PageSpikes and reorganizes it so the output is:
	  Key: "<language code>"
	  Value: "<page name> <max spike>"
	*/
	@Override
	    public void map(LongWritable key, Text value, Context context)
	    throws IOException, InterruptedException {
	    //We write the output key value pairs to the Context object
	    //This mapper reads the output of the first MapReduce as a whole record
	    //The entire record is stored in the value variable which has to be converted to string
	    String line = value.toString();

	    //value is split using "\t" as the delimiter
	    String split[] = line.split("\t");
	    //first split is stored in uniquePage
	    String uniquePage = split[0];
	    //second split is stored in maxSpike
	    String maxSpike = split[1];
	    
	    //first split is further split into two parts
	    String split2[] = uniquePage.split(" ");
	    //first part is the langName put into newKey
	    String newKey = split2[0];
	    //second part is the pageName. It is appended with the maxSpike and put into newValue
	    //Eg: <newKey, newValue> = <en, Main_Page 873562> 
	    String newValue = split2[1]+ " " + maxSpike; 
	    
	    //new Key and Value pairs are written onto the context object
	    context.write(new Text(newKey), new Text(newValue));
	}
    }
    

    //@author: Sakethram
    public static class TopPagesReduce
	extends Reducer<Text, Text, Text, Text> {
	//In the above statement, first two Text, Text indicates the input Key Value formats
	//The last two Text, Text indicates the output Key Value formats for the reducer
	
	/*
	  This reducer sorts pages in a particular language by their spike size using a tree.
	  output:
	  Key: <language code>
	  Value: print out of sorted tree map of N pages and their spikes from largest to smallest spikes, where N is the number of pages requested by the user with the number of unique pages in the language at the end
	*/
	@Override
	    public void reduce(Text key, Iterable<Text> values, Context context)
	    throws IOException, InterruptedException {

	    //Here the key is a particular language
	    //values contains the list of unique pages with their maximum spike during any O day period

	    //counter variable is used to find the top N spiked pages
	    //uniquePages variable is used to fing the #uniquePages in that language
	    int counter=0, uniquePages = 0;
	    //newValue holds the top N spiked pages
	    String newValue = "";

	    //HashMap is used to store the unique page name as key and its spike as the value
	    Map<String, Integer> topSpikes = new HashMap<String, Integer>();
	    for (Text val : values) { //for all unique pages in that language
		//incrementing the uniquePages in each iteration
		uniquePages++;
		//splits the composite value seperately
		String split[] = val.toString().split(" ");
		//topSpikes.put(split[0], Integer.parseInt(split[1]));//puting page and spike into hashmap with page as hashkey
		
		//If the HashMap's size is less than N, then directly insert the pagename into the table
		if(topSpikes.size()<= Integer.parseInt(context.getConfiguration().get("WikiSpikes_N"))){
		topSpikes.put(split[0], Integer.parseInt(split[1]));//putting page and spike into hashmap with page as hashkey
		}

		//compute the least spiked page from the HashMap, remove it and insert the current page
		else{
		    //Using an iterator to search the HashMap
			Iterator<Entry<String, Integer>> it = topSpikes.entrySet().iterator();
			//minSpikeKey & minSpikeValue contains the key & value of least spiked page
			//Initilize them to null
      			Integer minSpikeValue = null;
			String minSpikeKey=null;

			//Iterating the HashMap
			while (it.hasNext()) {
		        //pairs hold the current key value pair
		        Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>)it.next();
			//if the current pairs value is less than the existing value, then replace minSpikeValue
			//and its corresponding key
		        if(minSpikeValue == null || pairs.getValue() < minSpikeValue){
		        	minSpikeValue = pairs.getValue();
		        	minSpikeKey = pairs.getKey();
		        }
			}

			//By this point of time, minSpikeValue contains the least value in the HashMap
			//If minSpikeValue is less than current pages spike value, then remove the corresponding
			//page using minSpikeKey and insert the current page
		        if(Integer.parseInt(split[1]) > minSpikeValue){
		        	topSpikes.remove(minSpikeKey);		        	
				//putting page and spike into hashmap with page as hashkey
		         	topSpikes.put(split[0], Integer.parseInt(split[1]));     	
		        }
	          }
	    }
	    
	    //bvc has the property of sorting by values
	    ValueComparator bvc =  new ValueComparator(topSpikes);
	    //puting hashmap of pages into tree sorted by spike size value
	    //used TreeMap to sort the pages in HashMap based on their spike value
	    TreeMap<String,Integer> sorted_map = new TreeMap<String,Integer>(bvc);
	    
	    //putting values from HashMap to TreeMap that has the property of sorting by value
	    sorted_map.putAll(topSpikes);
	    
	    Iterator<Map.Entry<String,Integer>> it = sorted_map.entrySet().iterator();
	    
	    //This while loop is used to append the top N spiked pages to the newValue that has to be written to the context
	    //counter should be less than N
	    while (it.hasNext() && counter < Integer.parseInt(context.getConfiguration().get("WikiSpikes_N"))) {
		Map.Entry<String, Integer> entry = it.next();
		//catenates the top N pages and their values into a string
		newValue += entry.getKey() + " " + entry.getValue().toString() + " ";
		counter++;
	    }
	    
	    //appends the size of the hash, the number of unique pages in the language, to the end of the value string
	    newValue += uniquePages;
	    //writes the language name as key and top N pages with their spikes as values.
	    //newValue also contains the no of unique pages in that language
	    //For eg: if N is 3, <key, value> = <en, Main_Page 77845847 Jodie_Foster 6454 Special_Random 987 837465874
	    //Here 837465874 denotes the #uniquePages in en
	    context.write(key, new Text(newValue));
	}
    }
    
    //@author: Sakethram
    public static boolean main(Configuration conf, String[] otherArgs)
	throws Exception {
	
	//conf contains the command line arguments and other configuration settings
	//create new job
	Job job = new Job(conf, "Top Pages");
	//setting the class file to locate the relevant JAR file
	job.setJarByClass(PageSpikes.class);
	
	//setting the mapper class
	job.setMapperClass(TopPagesMap.class);
	//setting the mapper's key value formats
	job.setMapOutputKeyClass(Text.class);
	job.setMapOutputValueClass(Text.class);
	
	//setting the reducer class
	job.setReducerClass(TopPagesReduce.class);
	//setting the reducer key value types
	job.setOutputKeyClass(Text.class);
	job.setOutputValueClass(Text.class);
	
	//specifying the input and output directory paths
	//the input path is taken as the output path of the first MapReduce job
	FileInputFormat.addInputPath(job, new Path(otherArgs[1] + "/PageSpikes"));
	FileOutputFormat.setOutputPath(job, new Path(otherArgs[1] + "/TopPages"));

	//setting the number of reducer tasks
	job.setNumReduceTasks(48);
	
	//starts the job and waits until the job completes
	return job.waitForCompletion(true);
    }
}
/**
 * Class Name: PageSpikes.java
 * This is the First Map Reduce class which takes the input as raw data and outputs the maximum spike 
 * for a unique page in each language over any O day period. 
 * 
 * Map 1:
 * The Mapper class takes the input data and Hadoop splits that raw input data into different blocks of 
 * size of 128 MB. 
 *
 * Input: Key is an offset here and Value is the each record taken from an input file
 * Process i/p to give the o/p in this format:
 * Output:
 * Key: <<language_name>" "<page_name>>
 * Value: <<page_views><page_date>>
 * (Here pagedate consists of 4 digits with first two representing month and the next two representing day)
 * Eg:<Key><Value>
 * <aa Main_Page><<4536><0102>>
 * <en Cricket><<2903><0128>>
 * <fr FootBall><<34012><0219>> 
 *
 * Reduce 1: 
 * The pages become unique by now. Compute the max spike in any O day period (eg: max spike in any 5 day period over 60 days).
 * Input:
 * Key: <<language_name>" "<page_name>>
 * Value: <<page_views><page_date>>
 * (Notice that the output of Map is taken as the input of Reduce function)
 *
 * O/p format:
 * Key: lang pagename
 * Value: maxspike
 * 
 * Eg:<Key><Value>
 * <aa MoiraMoira><4490>
 * <da Jodi_Hoster><234561>
 * <en Alice><82132>
 *
 * @ authors: Ranga Reddy, Sakethram
 */


package team4.project1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

// @authors: Ranga Reddy, Sakethram
public class PageSpikes {
    
    // @author for PageSpikesMap: Sakethram
    public static class PageSpikesMap
    	extends Mapper<LongWritable, Text, Text, Text> {

	/*Map function takes input from the wiki log files one line at a time and filters out languages with non two-letter codes. 
	  out put key value pair is:
	  key: "<language code> <page name>"
	  value: "<pageviews><date>"
	*/
	String lang, fileName, date, pageViews;

	@Override
	public void map(LongWritable key, Text value, Context context)
	    throws IOException, InterruptedException {
	    
	    String pageName;
	    //the parameter "value" which is of type Text is converted into String
	    String line = value.toString(); 
	    //each record i.e line is split with the delimiter as " "(space)
	    String split[] = line.split(" "); 

	    /*  Eliminating bad records: 
	        after splitting the each record, it is broken into 4 parts and hence if it is not equal to the length of 4 then
	        the record is not processed further
	     */
	    if(split.length == 4) 
	    {	
		//the first data element in the record is a language. So, the language is stored in a "lang" variable
		lang = split[0];  

		//getting the name of input file
		fileName = ((FileSplit) context.getInputSplit()).getPath().getName(); 

		//extracting the date from the filename which is the substring of filename from positions 15-18 
		date = fileName.substring(15, 19);

		//retrieving the number of page views in each record and saving into the variable "pageViews"
		pageViews = split[2]; 

		//adding date at the end of pageViews variable
		pageViews = pageViews + date;  
	    
		//considering languages only with two letters i.e., with length 2
		if(lang.length()==2) { 

		    //storing the page name of the record into the variable "pageName"
		    pageName = split[1]; 

		    //add " "(space) to the lang variable and then append the value of "pageName" 
		    lang = lang + " " + pageName; 
	
		    /*write the values of <<language_name>" "<page_name>> as the "Key" eg: "en Main_Page"*/
		    /*write the values of <<number_of_page_access><date>> as the "Value" 
		    eg:"348950102" where 34895 is the number of page views and 0102 is the date */
		    context.write(new Text(lang), new Text(pageViews));

		}
	    }
	}
    }
    

    //@ authors for PageSpikesReduce Class: Ranga Reddy, Sakethram 
    public static class PageSpikesReduce
	extends Reducer<Text, Text, Text, IntWritable> {
	
	/*Reduce function takes output of mapper(key,value) : (<language> <page name>,<views><date>) and
	  outputs the key value pair:
	  key: "<language code> <page name>"
	  value: "<largest spike>"
	*/
	@Override
	    public void reduce(Text key, Iterable<Text> values, Context context)
	    throws IOException, InterruptedException {
	    int sum = 0;
	    //counter variable for processing O-day period(eg: for 1-5, 2-6..etc)
	    int counter; 

	    // dayKey variable is used as a key in the 'duplicate_check' HashMap variable
	    Integer dayKey, pageViews; 
	    String pageDate;
	    Integer max,min, finalMaxSpike;

	    //"key" of Text type is converted into String and stored into keyVal variable 
	    String keyVal = key.toString(); 

	    //language and pagename of the Reduce input Key are being split here
	    String split[] = keyVal.split(" "); 

	    //first part of key, 2 letter lang code
	    String keyVal2 = split[0];

	    //Creating a new ArrayList variable "periodSpikes" which stores Integer type values of MaxSpikes
	    ArrayList<Integer> periodSpikes = new ArrayList<Integer>();
	    
	    //creating a new HashMap variable "duplicate_check" which stores each day as a key and corresponding page spikes as values
	    Map<Integer, ArrayList<Integer>> duplicate_check = new HashMap<Integer, ArrayList<Integer>>();
	    
	    //Computation of spikes
	    //iterating all the input values of reduce function which is of the format "<number_of_page_access><date>"
	    for (Text val : values) {
		pageDate = val.toString();

		//splits pageviews from date from input Value
		pageViews = Integer.parseInt(pageDate.substring(0, (pageDate.length()-4)));
		
		//calls function getDay() with parameter pageDate and then the function returns day of year from month and day
		dayKey = getDay(pageDate);
		
		if(duplicate_check.containsKey(dayKey))
		    {//if a day is already entered in map, check for new min/max
			
			ArrayList<Integer> tempList = new ArrayList<Integer>();
			
			//retrieve already existing maximum and minimum values from the duplicate_check hashmap
			max = duplicate_check.get(dayKey).get(0);
			min = duplicate_check.get(dayKey).get(1);
			
			if(max < pageViews)
			    {	//checks for new maximum
				max = pageViews;
			    }
			if(min > pageViews)
			    {//checks for new minimum
				min = pageViews;
			    }
			
			//saving the max and min values to the arraylist variable "tempList"
			tempList.add(max);
			tempList.add(min);

			//if there is a new maximum or new minimum then overwrite previous values with these values
			duplicate_check.put(dayKey, tempList);
		    }	

		//if the day is not present in the duplicate_check hashmap
		else
		    {//add day to map with new min and max pageviews
			ArrayList<Integer> tempList = new ArrayList<Integer>();
			max = pageViews;
			min = pageViews;
			tempList.add(max);
			tempList.add(min);

			//the new key-value pairs are pushed into the duplicate_check hashmap
			duplicate_check.put(dayKey, tempList);
		    }
		
	    }
	    
	    //Computation of spike during 'O' day period
	    /*since the input data has to be processed within the first two months 
	      in 2013 the below for loop is iterated only for those 2months
	    If the input data is more than 2months then we have to modify the value here correspondingly*/
	    for(int j = 1; j <= 60; j++){
		ArrayList<Integer> periodList = new ArrayList<Integer>();
		
		//here the counter variable is used for calculating the spikes for each O-day period
		//Initially each day value which is variable 'j' is stored in the counter variable
		counter = j;
		
	       //Depends on 'O' day period. the command line argument 'O' is retrieved from the Configuration which is set in WikiSpikes class
		//the while loop iterates until the complete O-day duration is processed using the condition.
		while(counter < j+ Integer.parseInt(context.getConfiguration().get("WikiSpikes_O"))){
		    
		    /*if at all, the day is present in the duplicate_check hashmap 
		      then retrieve the maximum and minimum spikes and store in the periodList arraylist. */
		    if(duplicate_check.containsKey(counter)){
			periodList.add(duplicate_check.get(counter).get(0));
			periodList.add(duplicate_check.get(counter).get(1));
		    }
		    counter++;
		}

		//if periodList arraylist is not empty then find the maximum and minimum views in the list 
		if(!periodList.isEmpty()){
		    
		//finds the max views in current period
		Integer periodMax = Collections.max(periodList);

		//finds the min views in current period
		Integer periodMin = Collections.min(periodList);

		//calculates the spike by subtracting minimum views from maximum views and adds it to the periodSpikes list
		periodSpikes.add(periodMax - periodMin);		
		}
	    }
	    
	    if(!periodSpikes.isEmpty()){

	    //finds greatest spike from all the spikes in the periodSpikes list
	    finalMaxSpike = Collections.max(periodSpikes);
	    
	    //outputs language and page with it's greatest spike
	    context.write(key, new IntWritable(finalMaxSpike));
	    }
	    
	    
	}
	
	/*
	  takes month and day combinedly as a date string and converts to day of year.
	  ie jan 18: 0118 becomes 18
	  feb 8: 0208 becomes 39
	*/
	public int getDay(String pageDate){
	    //parses date string and splits into month and day
	    int month = Integer.parseInt(pageDate.substring(pageDate.length()-4, pageDate.length()-2));
	    int date = Integer.parseInt(pageDate.substring(pageDate.length()-2, pageDate.length()));
	    
	    //using the month variable, corresponding day is calculated here
	    switch(month){
	    case 1: return date;//jan
	    case 2: return 31+date;//feb
	    case 3: return 59+date;//march
	    case 4: return 90+date;//april
	    case 5: return 120+date;//may
	    case 6: return 151+date;//june
	    default: return 0;
	    }
	}
    }
    
    //@author: Ranga Reddy
    public static boolean main(Configuration conf, String[] otherArgs)
	throws Exception {
	
	//conf variable has the command line arguments specified in the Main class i.e WikiSpikes
	//creating a new job using the Job Class
	Job job = new Job(conf, "Page Spikes");
	
	//setting the class file to locate the relevant JAR file
	job.setJarByClass(PageSpikes.class);
	
	//set the mapper class
	job.setMapperClass(PageSpikesMap.class);

	//set the mapper output's Key-Value pair types
	job.setMapOutputKeyClass(Text.class);
	job.setMapOutputValueClass(Text.class);
	
	//set the reducer class
	job.setReducerClass(PageSpikesReduce.class);

	//set the reducer output's Key-Value pair types
	job.setOutputKeyClass(Text.class);
	job.setOutputValueClass(IntWritable.class);
	
	//specify the input and output directory paths
	FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	FileOutputFormat.setOutputPath(job, new Path(otherArgs[1] + "/PageSpikes"));

	//set the number of reduce tasks
	job.setNumReduceTasks(48);
	
	//starts the job and waits until the job completes
	return job.waitForCompletion(true);
    }
}
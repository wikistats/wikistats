/**
 * Class Name: TopLangs.java                                                                                                                 
 * This is the third MapReduce class which takes the input from second MapReduce and outputs the
 * top M languages and their top N spiked pages along with the #uniquePages in that language.                                                                             
 * Map 3:                                                  
 * The Mapper class takes the input data from the Reduce 2. Process i/p and makes the key same for all records. The value
 * is the entire record in the input file with minimal modifications.
 * Input:
 * Key: offset
 * Value: entire record/line in the input file
 *                                                                                                 
 * Output:                                                                                                                                     
 * Key: Final (some other key can also be used)                                                           
 * Value: lang_name pagename1 spike1 pagename2 spike2...........#uniquePages                                   
 * 
 * O/p Eg:<Key><Value>
 * Final  en Main_Page 78346 Jodie_Foster 76575 ..... 4983467573                                                                          
 * Reduce 2:                                                                          
 * Since the key used is same for all records, all the languages, their unique page count and their top N pages goes to the same reducer. 
 * Input:
 * Key: Final
 * Value: lang_name pagename1 spike1 pagename2 spike2...........#uniquePages
 * Output:
 * Top_Lang    #uniquePages
 * Page_name   max_spike
 * Page_name   max_spike
 * Page_name   max_spike
 *  
 *
 * O/p Eg:
 * en               231020040
 * Jodie_Foster     451141
 * Main_Page        395341
 * Colin_Kaepernick   208598
 * Special:Random     184358
 *
 *@authors: Sakethram, Ranga Reddy
*/

package team4.project1;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

//@authors: Sakethram, Ranga Reddy
class TopLangs{
	    
    //@author: Sakethram
    public static class TopLangsMap
	extends Mapper<LongWritable, Text, Text, Text> {
	//In the above statement, LongWritable, Text indicates the input Key Value formats
        //The last two Text, Text indicates the output Key Value formats for the mapper
        /*This map takes the output from TopPages and reorganizes it so the output is:                                                       
          Key: Final 
          Value: <lang_name> <page name> <max spike> <page name> <max spike>.....<#uniquePages>
	*/


	@Override
	    public void map(LongWritable key, Text value, Context context)
		    throws IOException, InterruptedException {
	    //We write the output key value pairs to the Context object 
	    //This mapper reads the output of the first MapReduce as a whole record
	    //The entire record is stored in the value variable which has to be converted to string
	    String line = value.toString();
	    //Replaces the "\t" with " "
	    String split[] = line.split("\t");
	    String newValue = split[0] + " " + split[1];
	    
	    //newKey is Final and new Value is as mentioned in the introductory comment
	    context.write(new Text("Final"), new Text(newValue));
	}
    }
    
    //@authors for TopLangsReduce Class : Ranga Reddy
    public static class TopLangsReduce
	extends Reducer<Text, Text, Text, Text> {
	//In the above statement, first two Text, Text indicates the input Key Value formats
	//The last two Text, Text indicates the output Key Value formats for the reducer
	/*This reducer sorts pages in a particular language by their spike size using a TreeMap. */
	@Override
	    public void reduce(Text key, Iterable<Text> values, Context context)
	    throws IOException, InterruptedException {
	    
	    //this counter is used to find the top M languages
	    int counter = 1;

	    //HashMap is used to store the language_name and page_names as key and lang_unique_page_count as value
	    Map<String, Integer> topLang = new HashMap<String, Integer>();
	    //for all languages in the list
	    for (Text val : values) {
		//converting the text to string
		String valStr = val.toString();
		//finding the last occurrence of " " to seperate the uniquePage value which
		//was appended at the last position in MR2
		int i = valStr.lastIndexOf(" ");
		//topLangKey contains the language_name and its pages as the key
		String topLangKey = valStr.substring(0, i);
		//topLangVal contains the language_unique_page_count
		String topLangVal = valStr.substring(i+1, valStr.length());
		
		//insert the key value pair into the HashMap
		topLang.put(topLangKey, Integer.parseInt(topLangVal));
		
	    }
	    
	    //bvc gives the property of sorting by value
	    ValueComparator bvc =  new ValueComparator(topLang);
	    //used a tree map to sort the languages based on their unique page count
	    TreeMap<String,Integer> sorted_map = new TreeMap<String,Integer>(bvc);
	    //putting the values from HashMap to TreeMap
	    sorted_map.putAll(topLang);
	    //all the languages are sorted by this point of time

	    //this iterator helps to run through the TreeMap
	    Iterator<Map.Entry<String,Integer>> it = sorted_map.entrySet().iterator();
	    
	    //counter should be less than or equal to top M languages
	    //this while loop is used to write the final output in the required format
	    while (it.hasNext() && counter <= Integer.parseInt(context.getConfiguration().get("WikiSpikes_M"))) {
		//it contains the current record in the TreeMap
		Map.Entry<String, Integer> entry = it.next();
		
		//gets the key from TreeMap and sets it to newKey
		String newKey = entry.getKey();
		//finding the first occurrence of " " to seperate the language name from the key
		int p = newKey.indexOf(" ");
		//setting the language to lang
		String lang = newKey.substring(0, p);
		//setting the remaining value to pageVal. It now contains the top N page_names along with their max_spikes
		String pageVal = newKey.substring(p+1, newKey.length());
		
		//pageVal is split using the delimiter " "
		String split[] = pageVal.split(" ");
		//split array contains the page_names and max_spikes alternatively
		//for eg, Main_Page 58947 Jodie_Foster 32245 Special:Random 23445
		for(int k = 0; k < split.length; k+=2){
		    //writing the page_name and its max_spike to the final output
		    context.write(new Text(split[k]), new Text(split[k+1]));
		}
		//writing the language name and its unique pagecount to the final output
		context.write(new Text(lang), new Text(entry.getValue().toString()));
		//couter stores the no of languages that are written to the final output till that point of time
		counter++;
	    }
	}
    }
    
    //@author: Sakethram
    public static boolean main(Configuration conf, String[] otherArgs)
	throws Exception {
	
	//conf variable has the command line arguments specified in the Main class i.e WikiSpikes
	//creating a new job using the Job class
	Job job = new Job(conf, "Top Languages");

	//setting the class file to locate the relevant JAR file
	job.setJarByClass(PageSpikes.class);
	
	//set the mapper class
	job.setMapperClass(TopLangsMap.class);
	
	//set the mapper output's Key-Value pair types
	job.setMapOutputKeyClass(Text.class);
	job.setMapOutputValueClass(Text.class);
	
	//set the reducer class
	job.setReducerClass(TopLangsReduce.class);
	
	//set the reducer output's Key-Value pair types
	job.setOutputKeyClass(Text.class);
	job.setOutputValueClass(Text.class);

	//specify the input and output directory paths
	FileInputFormat.addInputPath(job, new Path(otherArgs[1] + "/TopPages"));
	FileOutputFormat.setOutputPath(job, new Path(otherArgs[1] + "/TopLangs"));
	
	//starts the job and waits until the job completes
	return job.waitForCompletion(true);
    }
}
/****************************Introductory Comments*****************************************
 * Project Description: 
 * This project is about analyzing WikiStats data. The data contains Wikipedia page access 
 * information in thousands of files with millions of records in each file. Each file has the information 
 * for a particular hour on a particular day. Each record in the file contains information about the
 * page such as <language_of_the_page>, <page_name>, <#page_access_in_that_hour>, <page_size>.
 * 
 * Input Data Size Information:
 * 1 Hour: ~300MB
 * 1 Day: ~7.2GB
 * 1 Month (30 Days): ~216GB
 * 2 Months (60 Days): ~432GB
 * 
 * Terminology:
 * Spike: A spike is the difference between maximum and minimum page views at any particular hour
 * over a period of time.
 * Key Value Pair: The data is transmitted from mappers to reducers in the form of <Key, Value> pairs.
 * 
 * 
 * Parameters:
 * This project calculates the top N spiked pages in top M languages over any O day period from the
 * entire input data. Here M, N, O are the command line arguments that are passed along with the input
 * directory path.
 * For eg, M-5, N-10, O-5 values calculates the top 10 spiked pages in top M languages over any 5 day period.
 * Here top M languages are the ones which has the most number of unique pages over the entire input.
 * 
 * Design Strategy:
 * The problem is solved by splitting the task into 3 different MapReduce jobs. Below is a brief
 * description of what each MapReduce job does.
 * 
 * MapReduce Job 1:
 * This MapReduce reads the raw data and calculates the maximum spike for a particular page over any
 * O day period.
 * MapReduce Job 2:
 * It makes the languages unique and calculates the top N spiked pages in that language
 * using a HashMap. It also calculates the number of unique pages in that language.
 * MapReduce Job 3:
 * This final MapReduce job computes the top M languages based on the unique page count calculated in MR2
 * and writes it to the final output.
 *  
 *   
 * Team Members Contribution:
 * WikiSpikes (Main class): Ranga Reddy
 * Map-1 (PageSpikes Mapper): Sakethram
 * Reduce-1 (PageSpikes Reducer): Ranga Reddy, Sakethram
 * Map-2 (TopPages Mapper): Ranga Reddy
 * Reduce-2 (TopPages Reducer): Sakethram
 * Map-3 (TopLangs Mapper): Sakethram
 * Reduce-3 (TopLangs Reducer): Ranga Reddy
 * ValueComparator: Ranga Reddy
 * 
 * Instructions for running the project are  mentioned in a separate README file.
 *
 ****************************************************************************************
 * Class Name: WikiSpikes.java
 * This class is responsible for calling all the MapReduce jobs one after the other. It also
 * plays a key role in passing the command line arguments to all the jobs. This is done by setting
 * the parameter values to a configuration and passing that configuration. 
 */


package team4.project1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.GenericOptionsParser;

import team4.project1.*;

//@author:Ranga Reddy 
public class WikiSpikes {
    public static void main(String[] args) throws Exception {
	Configuration conf = new Configuration();

	//Retrieve the command line arguments and put it in the otherArgs string array
	String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

	//If there are no 5 parameters, then show an error message and exit the program
	if (otherArgs.length != 5) {
	    System.err.println("Usage: wikiStats <in> <out> <Number per language> <Number of languages> <Period in days>");
	    System.exit(2);
	}

	//Set the command line argument value for M top languages to "WikiSpikes_M"
	conf.setInt("WikiSpikes_M",Integer.parseInt(otherArgs[2]));
	//Set the command line argument value for N top top spiked pages to "WikiSpikes_N"
	conf.setInt("WikiSpikes_N",Integer.parseInt(otherArgs[3]));
	//Set the command line argument value for O day period to "WikiSpikes_O"
	conf.setInt("WikiSpikes_O",Integer.parseInt(otherArgs[4]));

	//If the first MapReduce doesn't return true, then quit the execution
	if(! PageSpikes.main(conf, otherArgs)){
	    System.exit(3);
	}
	//If the second MapReduce doesn't return true, then quit the execution
	if(! TopPages.main(conf, otherArgs)){
	    System.exit(4);
	}
	//If the third MapReduce doesn't return true, then quit the execution
	if(! TopLangs.main(conf, otherArgs)){
	    System.exit(5);
	}

    }
}

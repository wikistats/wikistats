/**
 * Class Name: ValueComparator.java
 * This class implements the Comparator interface which helps in a customized sort
 * The parameterized constructor for this class is implemented with map as a parameter and so while 
 * creating an object for this class, map is passed as a parameter which gets the compare method property of the Comparator
 *
 * By default, a TreeMap has the sort property which sorts the keys or values in ascending order. 
 * But for the project we need the values to be sorted in the descending order so that top N maximum spikes can be retrieved
 * So, we are using a customized sort with the help of Comparator interface.
 *
 * Also this code segment for sorting the values in a TreeMap in descending order  is grabbed from Stack Overflow website. 
 * Here is the source link of it:
 * Citation:- http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
 * 
 * @author: Ranga Reddy 
 */


package team4.project1;

// The Comparator interface is imported from this below package
import java.util.Comparator;
import java.util.Map;

//@author : Ranga Reddy
//Since the Comparator is an interface the ValueComparator has to implement the interface
public class ValueComparator implements Comparator<String>{

    Map<String, Integer> base;
    //create a parameterized constructor with Map as the parameter
    public ValueComparator(Map<String, Integer> base) {
	this.base = base;
    }

    // The compare method is implemented from the Comparator interface and since we want to sort in descending order of values of a Map
    // we use ">=" operator here
   public int compare(String a, String b) {
	if (base.get(a) >= base.get(b)) {
	    return -1;
	} else {
	    return 1;
	} 
    }
}
